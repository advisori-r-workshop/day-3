---
title: "R Workshop "
output:
  rmdformats::readthedown:
    highlight: pygments
    css: media/style.css
    df_print: paged
---  

We are going to discuss statistical methods mainly based on data arranged in tables. That's why using the tidyverse package for the data preparation is opportune.

The tidyverse package includes in particular the package ggplot2. Using this package allows for producing graphics efficiently.

```{r}
# !diagnostics off

library(tidyverse)
#library(ggplot2)
```

```{r}
MoviesData <- readr::read_csv("data/IMDB_1000movies_2006_2016.csv")
```

Examples for our analysis in the following are based on data in both numeric and alphanumeric form.

```{r}
colnames(MoviesData)
```

```{r}
head(MoviesData)
```

# Descriptive Statistics

## Univariate Analysis

For studying the distribution of a numeric variable we can generate a histogram easily with the qplot function. Here we use geom="histogram" as argument when calling the function.

```{r}
qplot(Year, data=MoviesData, geom="histogram", binwidth = 1, xlab="Year", main="Frequency distribution")
qplot(Rating, data=MoviesData, geom="histogram", bins = 20, xlab="Rating", main="Frequency distribution")
qplot(Votes, data=MoviesData, geom="histogram", bins = 20, xlab="Votes", main="Frequency distribution")
qplot(Metascore, data=MoviesData, geom="histogram", bins = 20, xlab="Metascore", main="Frequency distribution")
qplot(Runtime_Minutes, data=MoviesData, geom="histogram", bins = 20, xlab="Runtime (Minutes)", main="Frequency distribution")
qplot(Revenue_Millions, data=MoviesData, geom="histogram", bins = 20, xlab="Revenue (Millions)", main="Frequency distribution")
```

There is a summary function which can be applied to a variable (in a data frame). This function then calculates main statistics for the distribution of the variable.

Applying this function is meaningful for numeric variables. The summary function can be applied to all numeric variables of a data frame at once as follows:

```{r}
MoviesData %>% select_if(is.numeric) %>% summary()
```

> **Question** The values of diverse statistics for a variable can also be determined with the summarise function. What result gives this function in the following example?

```{r}
MoviesData %>% summarise( Mean_value = mean( Revenue_Millions ))
```

The summarise function can also be used for the calculation of statistics for a selected variable. This function is then used for calculations with scalar output like a mean value or a standard deviation though it is possible to calculate more than one such scalar output at once.

However, when calculating a vector of quantiles for a distribution (instead of a single quantile only) the quantile function shall be used on its own.

```{r}
MoviesData %>%
  summarise( Mean_value = mean( Revenue_Millions, na.rm=TRUE ),
             Standard_deviation = sd( Revenue_Millions, na.rm=TRUE ),
             Number_of_observations = n() )

MoviesData %>% summarise( Quartile3rd = quantile( Revenue_Millions, probs = c(0.75), na.rm=TRUE ))

quantile( MoviesData$Revenue_Millions, probs = c(0.9, 0.92, 0.95, 0.98, 0.99), na.rm=TRUE )
```

The summarise_if function can be used for doing calculations with a number of selected variables at once (e.g. all numeric variable of a data frame).

```{r}
MoviesData %>%
  summarise_if(is.numeric, median, na.rm=TRUE)
```

> **Question** We have excluded missing values from the calculations above. What else can we do with missing values?

```{r}
MoviesData %>%
  select_if(is.numeric) %>%
  replace_na( list( Revenue_Millions = 47.985,
                    Metascore = 59.5) ) %>%
  summary()
```

> **Try it** Replace the explicitly assigned values above with a dynamic assignment!

We can summarize the distribution of a variable graphically in form of a Box-Whisker-Plot. This plot can also be generated with the qplot function using geom="boxplot" as argument.

```{r}
qplot(1, Revenue_Millions, data=MoviesData, geom="boxplot", xlab="Rating", main="Box Whisker plot")
```

The Box Whisker plot shows 1) median (50th percentile), 2) lower/ upper quartile (25th / 75th percentile) 3) median minus/ plus 1.5 times the interquartile range, 4) outlier values.

The usage of packages like the moments package might be useful for further analysis of the distribution of a variable. Here functions for skewness and kurtosis are available.

The skewness (3rd moment) of a distribution describes an asymmetric shape of the distribution. Skewness is 0 for a symmetric distribution. Positive skewness indicates a long "tail" on the positive side of the distribution, while negative skewness indicates a long "tail" on the other side.

The kurtosis (4th moment) of a distribution also describes the shape of a distribution, namely the "tailedness" of a distribution. More precisely, higher kurtosis corresponds to greater extremity of deviations (outliers). The kurtosis of a distribution might be compared with the kurtosis a normal distribution.

```{r}
library(moments)

MoviesData %>% select(Revenue_Millions) %>% skewness(.,na.rm=TRUE)
MoviesData %>% select(Revenue_Millions) %>% kurtosis(.,na.rm=TRUE)
```

We can generate histograms with the ggplot function (instead of qplot as above). With the ggplot function we can overlay the distribution of a data sample with a Gaussian distribution (having the same mean and standard deviation as the sample data). Here we use ggplot with the statements geom_histogram and stat_function (for the overlay). The stat_function allows for the specification of a function where we use dnorm for the density of a Gaussian distribution. With this function specification a list of two parameter values is required.

```{r}
Par1st <- MoviesData %>% summarise( mean(Rating, na.rm=TRUE) ) %>% as.numeric()
Par2nd <- MoviesData %>% summarise( sd(Rating, na.rm=TRUE) ) %>% as.numeric()

ggplot(MoviesData, aes(x = Rating)) +
  geom_histogram(aes(y =..density..),
                 breaks = seq(0, 10, by = 0.5),
                 colour = "black",
                 fill = "white") +
  stat_function(fun = dnorm, args = list(mean = Par1st, sd = Par2nd), colour = "blue")
```

A normal probability plot can also be used to check whether a given data sample has approximately a Gaussian distribution. Here we use ggplot with the statements stat_qq and stat_qq_line (for the reference line).

```{r}
ggplot(MoviesData, aes(sample = Rating)) +
  stat_qq() +
  stat_qq_line(colour = "blue")
```

Here the x-axis represents the theoretical quantile values of a Gaussian distribution (having the same mean and standard deviation as the sample data), while the y-axis represents to the quantiles of the actual data. The reference line corresponds to a Gaussian distribution of the data.

> **Try it** Compare the distribution of the revenues with a Gaussian distribution! How to describe the deviation between the theoretical distribution and the distribution of the actual data?

The count function can be used for both numeric and alphanumeric variables.

```{r}
MoviesData %>% count(Year)

MoviesData %>% filter( Genre == "Drama" ) %>% count(Genre)

MoviesData %>% count(Director) %>% arrange(desc(n)) %>% head()
```

We can generate a pie chart for frequencies with the ggplot function. Here we use the statement geom_bar in combination with the statement coord_polar as follows:

```{r}
MoviesData %>% count(Year) %>%
  ggplot( aes(x="", y=n, fill=Year) ) +
  geom_bar(stat="identity", width=1) +
  coord_polar("y", start=0) +
  theme_void()
```

To calculate statistics separately for subsets of our data we combine the summarise function with a group_by. Here the argument for group_by is the variable the values of which define the relevant subsets for the calculation.

```{r}
MoviesData %>%
  group_by(Year) %>%
  summarise( Mean_value = mean( Revenue_Millions, na.rm=TRUE ),
             Standard_deviation = sd( Revenue_Millions, na.rm=TRUE ) )
```

Now we can generate a pie chart displaying an aggregated value of a variable for the subsets of the data.

```{r}
MoviesData %>%
  group_by(Year) %>%
  summarise( Revenue = sum( Revenue_Millions, na.rm=TRUE ) ) %>%
  ggplot( aes(x="", y=Revenue, fill=Year) ) +
  geom_bar(stat="identity", width=1) +
  coord_polar("y", start=0) +
  theme_void()
```

Analysing the data for missing values and other special values (implausible values) can be done as follows:

```{r}
MoviesData %>% select(Revenue_Millions) %>% summarise( sum( is.na(.) ) )
MoviesData %>% select(Revenue_Millions) %>% summarise( sum( !is.na(.) ) )

MoviesData %>% select(Revenue_Millions) %>% summarise(sum( . == 0 ))
MoviesData %>% select(Revenue_Millions) %>% summarise(sum( . == 0, na.rm=TRUE ))
MoviesData %>% filter( Revenue_Millions == 0 ) %>% count()
```

Implausible values might be deleted using the replace function.

```{r}
MoviesData_amended <- MoviesData %>%
  mutate( Revenue_Millions = replace( Revenue_Millions, Revenue_Millions == 0, NA ) )

MoviesData_amended %>% summarise( min( Revenue_Millions, na.rm=TRUE ) )
```

> **Try it** What else can we do with implausible values?

## Multivariate Analysis

The cor function is capable of calculating a product moment correlation (aka Pearson correlation). In this case the argument method="pearson" is used. When calling this function at least 2 variables have to be passed. The argument use="pairwise.complete.obs" specifies further details (how to deal with missing values).

```{r}
MoviesData_amended %>%
  select(Runtime_Minutes, Revenue_Millions) %>%
  cor(., use="pairwise.complete.obs", method="pearson")
```

Passing more than 2 variables is possible to calculate pairwise correlations for all combinations of pairs of the variables. In this way a correlation matrix is generated.

Pearson correlation is a measure for linear dependancy between variables. On the other hand, rang correlation coefficients are not restricted to measuring linear dependancies.

In particular, the Spearman correlation coefficient can be calculated with the cor function. For this purpose the argument method="spearman" is specified.

```{r}
MoviesData_amended %>%
  select(Rating, Metascore, Revenue_Millions) %>%
  cor(., use="pairwise.complete.obs", method="spearman")
```

The pairs function can be applied for a number of variables. Then this function generates a matrix consisting of scatterplots for each combination of pairs of the variables.

```{r}
pairs( ~ Rating + Metascore + Revenue_Millions, data = MoviesData_amended, labels = c("Rating", "Metascore", "Revenue") )
```

# Linear models

A linear regression model is a model with a response variable $y$ as output and a number of of predictor variables $x_1, x_2,\ldots , x_n$ as input. Here we consider a quantitative response variable, while predictor variables might be quantitative or categorical. The relationship between the response variable on the predictor variables is assumed to be given by a linear prediction function
$$y = a_0 + a_1 x_1 + a_2 x_2 + \ldots + a_n x_n$$
Here the coefficients $a_0, a_1,\ldots , a_n$ have to be determined.

Determining coefficients is based on the data for the response variable and the predictor variables with a number $m$ of observations is available. Then the equations for the linear model are given by
$$y = a_0 + a_1 x_{1,i} + a_2 x_{2,i} + \ldots + a_n x_{n,i} + e_i$$
for $i = 1,\ldots , m$.

The problem consists in minimizing the residuals $e_i$ (in an appropriate way). In case of a linear model the least squares method is typically used for this task.

The lm function is available for executing a linear regression (keeping the programmer's efforts at a minimum):

```{r}
Model1 = lm( Revenue_Millions ~ Votes, data = MoviesData_amended)

summary(Model1)
```

The return value of the lm function is an object of the class lm which has a number of components, first and foremost a component containing the coefficients of the model.

```{r}
class(Model1)

names(Model1)
```

The components of the lm object can be assessed directly or a function can be applied on the object to get a specific result.

```{r}
Model1$coefficients
```

```{r}
coef(Model1)
```

The ggplot function can be used to illustrate the results of a linear regression. Here we apply the statement geom_point for plotting actual values of the response variable against values of the predictor variable. The additional statement geom_line is then used for drawing a regression line.

As an alternative to geom_line the statement stat_smooth might be used. Then a regression line with confidence intervals is displayed.

```{r}
ggplot(MoviesData_amended, aes(x = Votes, y = Revenue_Millions)) +
  geom_point() +
  geom_line(data = fortify(Model1), aes(x=Votes,y=.fitted), colour="blue") +
  labs(x = "Votes", y = "Revenue (Millions)")

ggplot(MoviesData_amended, aes(x = Votes, y = Revenue_Millions)) +
  geom_point() +
  stat_smooth(method = "lm", colour="blue") +
  labs(x = "Votes", y = "Revenue (Millions)")
```

In the first example the fortify function is used to produce a data frame object from an object of the type lm.

Next we have a look at the output values as predicted by the model. Comparing predicted values with the real values given by the training data we have to pay attention to the details.
Predicted values (fitted values) can be calculated for each observation provided that a value for the input variable is available. Indeed there are missing values for the input variable so that calculation of predicted values is only possible for a subset of the given data. When using the ggplot function this problem is dealt with in the background.

```{r}
ggplot(data = fortify(Model1), aes(x=Revenue_Millions, y=.fitted)) +
  geom_point() +
  labs(x = "Actual Revenue (Millions)", y = "Predicted Revenue (Millions)")
```

We plot the residuals of a linear model against the fitted values as follows:

```{r}
ggplot(data = fortify(Model1), aes(x=.fitted, y=.resid)) +
  geom_point() +
  labs(x = "Predicted values", y = "Prediction error")
```

Linear regression with more than 1 predictor variable is executed as follows:

```{r}
Model_2var = lm( Revenue_Millions ~ Rating + Votes, data=MoviesData_amended)

summary(Model_2var)
```

It might be a good idea to standardise the input variables before including them into a model. For this purpose we can define a function for modifying variables and use this function with a mutate or mutate_at statement as follows.

```{r}
standardise <- function(x) {
  (x - mean(x)) / sd(x)
}

Model_2standardised <- MoviesData_amended %>%
  mutate_at( vars(Rating, Votes), list(standardise) ) %>%
  lm( Revenue_Millions ~ Rating + Votes, data=. )

summary(Model_2standardised)
```

> **Question** How can input variables be transformed in some other way?

We can also set up a model with an interaction term (that is equivalent to that for 2 input variables the product of this variables is added as a third input variable). When using the lm function this is easily done by replacing the + symbol with *.

```{r}
Model_2interaction = lm( Revenue_Millions ~ Rating * Votes, data=MoviesData_amended)

summary(Model_2interaction)
```

In fact linear models can do more than modeling linear relationships because input variables can be transformed. For example, to include a variable x in a model in quadratic form the notion I(x^2) is used.

```{r}
Model_2var2 = lm( Revenue_Millions ~ I(Rating^2) + Votes, data=MoviesData_amended)

summary(Model_2var2)$r.squared
```

Analysis of variance is possible with the anova function. We can use this function to quantify the extent to which a 2 variable model is superior to a 1 variable model. The anova function performs a hypothesis test comparing two linear models. The null hypothesis is that the two models fit the data equally well. The alternative hypothesis is that the second model is better (significantly better in the statistical sense). More precisely an F statistic is calculated and then on the basis of an associated p-value the null hypothesis might be rejected.

```{r}
anova(Model1, Model_2var2)
```

A low p-value provides evidence that the second model is better.

In our data there are alphanumeric variables like Actors which contain multiple values inside of one string. Usually such variables have to be modified before they can be used as input for a model.

It might be decided to extract one of these multiple values for further analysis. The function word is available in the stringr package for this purpose. Here we have to specify the value or the values (first, second, ...) to be extract from the string and the delimiter symbol.

```{r}
library(stringr)

MoviesData_extended <- MoviesData_amended %>%
  mutate(MainActor = word(Actors, start = 1L, end = 1L, sep = fixed(",")))

MoviesData_extended %>% count(MainActor) %>% arrange(desc(n)) %>% head()
```

The function str_detect of the stringr package is used for searching in a string. This can also be applied for simplifying an alphanumeric variable before including it into a model.

```{r}
MoviesData_extended %>% count(Genre) %>% arrange(desc(n)) %>% head()

MoviesData_extended <- MoviesData_extended %>%
  mutate(Selected_Genre_Action = str_detect(Genre, pattern = "Action", negate = FALSE)) %>%
  mutate(Selected_Genre_Comedy = str_detect(Genre, pattern = "Comedy", negate = FALSE)) %>%
  mutate(Selected_Genre_Drama = str_detect(Genre, pattern = "Drama", negate = FALSE)) %>%
  mutate(PrimaryGenre = case_when(Selected_Genre_Drama == TRUE ~ "Drama",
                                  Selected_Genre_Action == TRUE ~ "Action",
                                  Selected_Genre_Comedy == TRUE ~ "Comedy",
                                  TRUE ~ "Other"))

MoviesData_extended %>% count(PrimaryGenre) %>% arrange(desc(n))
```

> **Question** What is the difference between new column PrimaryGenre on the one hand and columns Selected_Genre_Action, Selected_Genre_Comedy, Selected_Genre_Drama on the other hand?

A histogram (barplot) for an alphanumeric variable can be generated with the ggplot function. Here the statement geom_bar is used.

```{r}
ggplot(data=MoviesData_extended, aes(x=PrimaryGenre)) +
  geom_bar()
```

When calling the lm function an alphanumeric variable can be used in the parameter setting like a numeric variable. However, an alphanumeric variable cannot be included in the model in the same way as a numeric variable. Indeed, the alphanumeric variable is automatically split into a number of indicator variables by the lm function.

```{r}
Model_qual0 = lm( Revenue_Millions ~ PrimaryGenre, data=MoviesData_extended )
summary(Model_qual0)
```

If a variable is split into indicator variables then the number of indicator variables is one less than the number of values of the variable. Accordingly, one of the values of the variables can be ignored when introducing the indicator variables. Which value shall be ignored can be modified with a contrasts argument in the function call.

```{r}
Model_qual1 = lm( Revenue_Millions ~ PrimaryGenre, data=MoviesData_extended, contrasts = list(PrimaryGenre = "contr.SAS") )

summary(Model_qual1)
```

# Classification

```{r}
StockMarketIndexes <- readr::read_csv("data/StockMarketIndexes_20200201.csv")
head(StockMarketIndexes)
```

We can plot a time series with the ggplot function. Here we use the geom_line statement.

```{r}
ggplot(StockMarketIndexes, aes(x = Date, y = Close_Stoxx) ) + geom_line()
```

It's possible to generate more than one plot at the same time. This can be done by adding a facet_wrap statement to ggplot.

```{r}
StockMarketIndexes_Long <- StockMarketIndexes %>% 
  tidyr::pivot_longer(cols=starts_with("Close")) %>%
  mutate(name = stringr::str_remove(name, "^Close_"))

StockMarketIndexes_Long %>%
  ggplot(aes(Date, value)) +
  geom_line() + 
  facet_wrap(~name, scales="free_y")
```

Calculating a Pearson correlation coefficient is meaningful for time series as stock price quotes (stock index quotes, resp.).

```{r}
StockMarketIndexes %>% select(Close_Stoxx, Close_FTSE, Close_FAZ) %>% cor(., use="pairwise.complete.obs", method="pearson")
```

The analysis of time series almost always requires some transformation of the data, for example difference calculation.

We simplify the time series representing the target variable such that we distinguish only between up and down in comparison with the previous value. In this way we arrive at a classification problem (instead of a linear model).

We calculate relative growth for the time series used as predictor variables.

```{r}
lg1 <- function(x)c(NA, x[1:(length(x)-1)])

df1 <- function(y){ ( y - lg1(y) ) / lg1(y) }

StockMarketIndexes <- StockMarketIndexes %>% 
  mutate_at(vars(Close_Stoxx, Close_FTSE, Close_FAZ), list("Diff" = df1)) %>% 
  mutate(Direction_Stoxx = case_when(
                              Close_Stoxx_Diff >= 0 ~ 1,
                              Close_Stoxx_Diff < 0 ~ 0
                            ))
head(StockMarketIndexes)
```

> **Question** Do you prefer some other transformation of the time series?

A logistic regression model is a model where the response variable has 2 or more than 2 admissible values. We have modified our data such that there are 2 admissible values for the response variable denoted by 0 and 1. A logistic regression model has a number of predictor variables.

In view of that the response variable has the values 0 and 1 only a function is used in the model such that the range of this function is restricted to the interval $[0,1]$. More precisely the logistic function $F: [-\infty,\infty] \rightarrow [0,1]$ defined by
$$t \rightarrow \frac{1}{1+exp(-t)} $$
is used. The relationship between the response variable $y$ and the predictor variables $x_1,x_2,\ldots ,x_n$ is then given by a predictor function
$$y = F(a_0 + a_1 x_1 + a_2 x_2 + \ldots + a_n x_n) $$
that is,
$$y = \frac{1}{1+exp(-a_0 - a_1 x_1 - a_2 x_2 - \ldots - a_n x_n)} $$
Here the coefficients $a_0, a_1,\ldots , a_n$ have to be determined. Determining coefficients is based on the data for the response variable and the predictor variables with a number $m$ of observations available.

The function glm can be used for generalized linear models, which is a class of models including the logistic regression model. Then for this function the arguments family = binomial and link = "logit" are used for specifying logistic regression.

```{r}
LogModel1 <- glm(formula = Direction_Stoxx ~ Close_FAZ_Diff, data = StockMarketIndexes, family = binomial(link="logit"))

summary(LogModel1)
```

The return value of the glm function is an object of the class glm which has a number of components, first and foremost a component containing the coefficients of the model.

```{r}
class(LogModel1)

names(LogModel1)
```

The predict function can be used to predict a probability that the target variable is equal to 1. Here we use type="response" as argument to obtain a probability as return value.

Then for comparing predicted values and actual values of the training data we might classify the probability output, e.g. by predicting an outcome of 1 if the probability is larger than 0.5. Then we can calculate a hit rate for the model etc.

```{r}
LogModel1_Analysis <- StockMarketIndexes %>%
  mutate( Predicted_Response = predict(LogModel1, newdata=., type="response") ) %>%
  mutate( Predicted_Response01 = 1 * ( Predicted_Response > 0.5 ) )

LogModel1_Analysis %>%
  summarise( Hit_rate = mean(Predicted_Response01 == Direction_Stoxx, na.rm=TRUE) )

LogModel1_Analysis %>%
  group_by(Predicted_Response01, Direction_Stoxx) %>%
  summarise(Frequency=n()) %>%
  drop_na()
```

Logistic regression with more than 1 predictor variable is executed as follows:

```{r}
LogModel2 <- glm(formula = Direction_Stoxx ~ Close_FAZ_Diff + Close_FTSE_Diff, data = StockMarketIndexes, family = binomial(link="logit"))

summary(LogModel2)
```

For a logistic regression model a relatively low value for the deviance indicates relatively good model fit. When studying two logistic regression models with the same target variable a direct comparison of the deviance of these models is meaningful.

In particular, we can compare the deviance of a model with the deviance of the so-called null model, that is a model with an intercept only (but no predictor variables).

```{r}
glm(formula = Direction_Stoxx ~ 1, family = binomial, data = StockMarketIndexes) %>% deviance()
```

McFadden's R squared is related to the comparison of a model with the null model. We get a value in the interval $[0, 1]$ where a high value for McFadden's R squared indicates good model fit.

```{r}
McFadden_R_sq <- function(LogModel){
  1 - summary(LogModel)$deviance / summary(LogModel)$null.deviance
}

McFadden_R_sq(LogModel1)
McFadden_R_sq(LogModel2)
```

We might classify the probability output of a logistic model, that is, we predict an outcome of 1 if the probability is larger than a certain threshold value. A wrong prediction of an outcome of 1 will be less likely if the threshold value is set to a higher value. But a high threshold value has the effect that a wrong prediction of an outcome of 0 is more likely.

A ROC curve is a popular graphic for simultaneously displaying the two types of errors for all possible thresholds. It traces out the two types of error as we vary the threshold value for the prediction. The actual thresholds are not displayed in the plot. The true positive rate is the sensitivity, that is, the fraction of an outcome of 0 that are correctly predicted when using a given threshold. The false positive rate is the fraction of an outcome of 1 that we classify incorrectly when using that same threshold.

A ROC curve can be generated with a combination of the functions prediction and performance which are available in the ROCR package. Here  vectors of the predicted response values and actual values of the target variable are required as input for the prediction function.

The performance function is applied subsequently on the output obtained from the prediction function. When calling the performance function the arguments measure="tpr" and x.measure="fpr" are relevant for generating a ROC curve (where "tpr" stand for "true positive rate" and "fpr" for "false positve rate"). The plot function is finally applied on the output of the performance function.

```{r}
library(ROCR)

Prediction1 <- ROCR::prediction( LogModel1_Analysis$Predicted_Response, LogModel1_Analysis$Direction_Stoxx )

Performance1 <- ROCR::performance(Prediction1, measure = "tpr", x.measure = "fpr")
plot(Performance1)

AUC1 <- ROCR::performance(Prediction1, measure = "auc")
AUC1@y.values
```

The overall performance of a classification is given by the area under the ROC curve (AUC). The ideal ROC curve rises steeply so that the larger the AUC the better the classification. The ROC curve no bettern than chance is expected to have an AUC of 0.5.

The AUC can be calculated with a combination of the functions prediction and performance. When calling the performance function the argument measure="auc" shall be used.

A probit regression (instead of a logistic regression) can also be executed with the glm function. In this case the argument link="probit" shall be set.

```{r}
ProbitModel1 <- glm(formula = Direction_Stoxx ~ Close_FAZ_Diff, data = StockMarketIndexes, family = binomial(link="probit"))
summary(ProbitModel1)
```

> **Try it** Calculate the hit rate of the probit model! Compare the predictions of the logit model with the predictions of the probit model!

# Decision trees

Tree-based methods can be used for classification problems with a target variable and a number of predictor variables. A tree is a set of splittung rules that are used to segment the range of values for the predictor variables. More precisely, the tree consists of nodes where each node represents a splitting rule depending on an attribute related to the predictor variables. Each branch of the tree represents the outcome of the split. Each leaf of the tree (terminal node) represents a class label.

There is a tree package for constructing decision trees.

In our first example we have a target variable modified such that we distinguish between two classes (high and low).

```{r}
library(tree)

MoviesData <- MoviesData %>%
  mutate(High_Revenue = case_when(
                              Revenue_Millions >= 50 ~ 1,
                              TRUE ~ 0
                            ))

tree1 <- tree(High_Revenue ~ Votes + Rating + Metascore + Runtime_Minutes + Year, data=MoviesData)

summary(tree1)
```

The summary function lists in particular the variables that are used as nodes in the tree and the number of leafs. The tree can be graphically displayed using the plot function followed by the text function (for display of the node labels).

```{r}
plot(tree1)
text(tree1)
```

```{r}
CarsData <- readr::read_csv("data/Car_Features.csv")
head(CarsData)
```

Tree-based methods can also be used for regression.

```{r}
tree2 <- tree(MSRP ~ Engine_HP + Engine_Cylinders + highway_MPG + city_MPG + Year, data=CarsData)
summary(tree2)
```

```{r}
plot(tree2)
text(tree2)
```

# Subset selection methods

Subset selection methods are approaches for identifying a subset of predictors that are on our shortlist of predictors. We aim for fitting a regression model on the selected subset of predictors. The subset selection methods include "best subset" and "stepwise selecetion".

## Best subset selection

For executing best subset selection, a regression model is fitted for each possible combination of the predictors on the shortlist. Then we inspect all of the resulting models for identifying the one that is best.

The selection of the best model from such a large set of models is usually broken up into two stages. First, for $k=1,2,\ldots$, we select the best model that contains exactly $k$ predictors. Then we inspect the models selected for $k=1,2,\ldots$ for identifying the model that is the best of the whole lot.

Here for the definition of the best model we refer to adjusted R squared or some other criteria.

We have to choose the variables for the shortlist of predictors, and then we decide on the details how these variables shall be prepared for analysis.

```{r}
CarsData %>% count(Driven_Wheels) %>% arrange(desc(n))
CarsData %>% count(Transmission_Type) %>% arrange(desc(n))
CarsData %>% count(Vehicle_Style) %>% arrange(desc(n))

CarsData <- CarsData %>%
  mutate(All_Wheel_Drive = case_when(Driven_Wheels == "all wheel drive" ~ 1,
                                  Driven_Wheels == "four wheel drive" ~ 1,
                                  TRUE ~ 0) ) %>%
  mutate(Automatic_Transmission = case_when(Transmission_Type == "AUTOMATIC" ~ 1,
                                 Transmission_Type == "MANUAL" ~ 0,
                                  TRUE ~ 0.5) ) %>%
  mutate(Convertible_Style = case_when(Vehicle_Style == "Convertible" ~ 1,
                                 Vehicle_Style == "Convertible SUV" ~ 1,
                                  TRUE ~ 0) )

CarsData %>% count(All_Wheel_Drive) %>% arrange(desc(n))
CarsData %>% count(Automatic_Transmission) %>% arrange(desc(n))
CarsData %>% count(Convertible_Style) %>% arrange(desc(n))
```

We have to deal with missing values, e.g. exclude all data records with missing values as follows:

```{r}
sum( is.na(CarsData) )
CarsData_proper <- na.omit(CarsData)
sum( is.na(CarsData_proper) )
```

The leaps package has a function regsubsets which we can use for best subset selection. When calling this function we pass a formula that contains all predictors on the shortlist. We use the argument nvmax to specify the maximum number of predictors to be included into a model.

```{r}
library(leaps)

Best_Model <- regsubsets(MSRP ~ Engine_HP + Engine_Cylinders + All_Wheel_Drive + Automatic_Transmission + Number_of_Doors + Convertible_Style + highway_MPG + city_MPG + Popularity + Year, nvmax=10, data = CarsData_proper)

summary(Best_Model)
```

The output of the summary function indicates with * the predictors that are included in a model with a certain number of predictors.

```{r}
qplot(Engine_HP, MSRP, data=CarsData_proper, colour=Engine_Cylinders)
```

> **Try it** Remove outlier values and plot the data again!

```{r}
names(summary(Best_Model))
```

For $k=1,2,\ldots$, we get the maximum values of R squared and adjusted R squared for those models that contains exactly $k$ predictors as follows:

```{r}
summary(Best_Model)$rsq
summary(Best_Model)$adjr2
which.max(summary(Best_Model)$adjr2)
```

We can illustrate the results for adjusted R squared using the plot function as follows:

```{r}
plot(summary(Best_Model)$adjr2, xlab="Number of variables", ylab="Adjusted R squared")
```

The selection of predictors can also be illustrated graphically as follows:

```{r}
plot(Best_Model, scale="r2")
plot(Best_Model, scale="adjr2")
```

In particular, we are interested in the coefficients of the best model according to best subset selection.

```{r}
coef(Best_Model, 8)
```

## Forward and backward stepwise selection

Best subset selection cannot be applied with a large number of predictors on the shortlist for computational reasons. In view of this we might consider stepwise selection which is computationally more efficient.

Forward stepwise selection begins with a model with no predictor and then adds predictors to the model step by step. In each step the additional predictor is selected such that a maximum improvement for the model is achieved. This selection procedure runs until all predictors on the shortlist are included in the model (or a specified maximum number of predictors are included in the model).

The function regsubsets of the leaps package is also capable of forward stepwise selection. Here we use the argument method="forward" when calling the function.

```{r}
Forward_Model <- regsubsets(MSRP ~ Engine_HP + Engine_Cylinders + All_Wheel_Drive + Automatic_Transmission + Number_of_Doors + Convertible_Style + highway_MPG + city_MPG + Popularity + Year, data=CarsData_proper, method="forward")

summary(Forward_Model)
```

The output of the summary function indicates with * the predictors that are included in a model with a certain number of predictors.

Backward stepwise selection begins with a model that contains all predictors on the shortlist and then removes predictors from the model step by step. In each step the least useful predictor is removed.

The function regsubsets is also capable of backward stepwise selection. Here we use the argument method="backward" when calling the function.

```{r}
Backward_Model <- regsubsets(MSRP ~ Engine_HP + Engine_Cylinders + All_Wheel_Drive + Automatic_Transmission + Number_of_Doors + Convertible_Style + highway_MPG + city_MPG + Popularity + Year, data=CarsData_proper, method="backward")

summary(Backward_Model)
```

The output of the summary function indicates with * the predictors that are included in a model with a certain number of predictors.
